var webdriverio = require('webdriverio');
var expect = require('expect');
var assert = require('assert');
var server = 'http://holod.phoenix-cg.ru';
var options = {
    desiredCapabilities: {
      'browserstack.local' : 'true'
    }
};



// TESTS:

// test 1: subbmitting form on main page

var searchPage = {
  form: '.about-form-wrap .j-feed-ajax',
  textInput: '.about-form-wrap input[type=text]',
  phoneInput: '.about-form-wrap input[type=tel]',
  msgArea: '.about-form-wrap textarea'
}

var form = '.about-form-wrap .j-feed-ajax';

describe('Opening the page', () => {
  it('- should open the page', () => {
    browser.url(server);
    browser.timeouts('script', 60000);
    browser.pause(1000);
    browser.setValue(searchPage.textInput, 'test');
    browser.setValue(searchPage.phoneInput, 89899879596);
    browser.setValue(searchPage.msgArea,'test text');
    browser.saveScreenshot();
    browser.pause(1000);
    browser.submitForm(form);

  });
});
